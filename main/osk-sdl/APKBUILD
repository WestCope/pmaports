# Maintainer: Clayton Craft <clayton@craftyguy.net>
# Co-Maintainer: Oliver Smith <ollieparanod@postmarketos.org>
#
# Note: cannot upstream to Alpine until:
# https://gitlab.alpinelinux.org/alpine/apk-tools/-/issues/10712
pkgname=osk-sdl
pkgver=0.65
pkgrel=0
pkgdesc="Onscreen keyboard for unlocking LUKS devices"
url="https://gitlab.com/postmarketOS/osk-sdl"
arch="all"
license="GPL-3.0-or-later"
depends="
	cryptsetup-libs
	directfb
	mesa-gl
	mesa-gles
	ttf-dejavu
	"
makedepends="
	cryptsetup-dev
	linux-headers
	meson
	scdoc
	sdl2-dev
	sdl2_ttf-dev
	"
source="https://gitlab.com/postmarketOS/osk-sdl/-/archive/$pkgver/osk-sdl-$pkgver.tar.gz"
options="!strip !check" # No tests
subpackages="$pkgname-doc"
provides="postmarketos-fde-unlocker"
provider_priority=1000

build() {
	abuild-meson build
	meson compile ${JOBS:+-j ${JOBS}} -C build
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C build

	# needed to trigger initfs rebuild:
	touch osk-sdl
	install -Dm644 osk-sdl \
		"$pkgdir"/usr/share/postmarketos-mkinitfs-triggers/osk-sdl
}

sha512sums="
070dcc1073266ecbe4c4c0089f29ac8687d98bbdcbd1c1578aa737bd6fbc96adeab6bd958295d073ef95515e63f536cef67c7e7e7b492b4ee3dc81fdcc8b9464  osk-sdl-0.65.tar.gz
"
